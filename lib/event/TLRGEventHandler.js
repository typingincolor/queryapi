var Logger = require('./../util/Logger').Logger;

function TLRGEventHandler() {
  this.handle = function(x, emitter, url, params) {
    var logger = new Logger();
    x.on('success', function(data) {
      logger.info('success', url, params);
      emitter.emit('success', data);
    }).on('timeout', function(ms) {
      logger.error('timeout', url, params);
      emitter.emit('timeout', {
        timeout: ms
      });
    }).on('fail', function(data) {
      logger.error('fail', url, params);
      emitter.emit('fail', data);
    }).on('error', function(data) {
      logger.error('error', url, params);
      emitter.emit('error', {
        url: url,
        error: data
      });
    });
  };
}

exports.TLRGEventHandler = TLRGEventHandler;
