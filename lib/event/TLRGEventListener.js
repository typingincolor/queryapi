function TLRGEventListener(emitter) {
  var that = this;

  this.success = function(f) {
    emitter.on('success', f);
    return that;
  };

  this.fail = function(f) {
    emitter.on('fail', f);
    return that;
  };

  this.timeout = function(f) {
    emitter.on('timeout', f);
    return that;
  };

  this.error = function(f) {
    emitter.on('error', f);
    return that;
  };
}

exports.TLRGEventListener = TLRGEventListener;
