var EventEmitter = require('events').EventEmitter;
var TLRGEventListener = require('./../event/TLRGEventListener').TLRGEventListener;
var TLRGEventHandler = require('./../event/TLRGEventHandler').TLRGEventHandler;
var Logger = require('./../util/Logger').Logger;
var util = require('util');
var _ = require('underscore');

function RatesInterface(url, rest, tlrgAppId) {
  this.getRates = function(id, params) {
    if (!_(id).isNumber()) {
      throw new Error('id must be a number');
    }

    var ratesUrl = util.format('%s/hotel/%d/rates', url, id);
    var emitter = new EventEmitter();
    var listener = new TLRGEventListener(emitter);
    var logger = new Logger();
    var handler = new TLRGEventHandler();

    logger.info('starting', ratesUrl);
    var restResult = rest.get(ratesUrl, {
      timeout: 5000,
      headers: {
        'TLRG-AppId': tlrgAppId
      },
      query: params
    });

    handler.handle(restResult, emitter, ratesUrl, params);
    return listener;
  };
}

exports.RatesInterface = RatesInterface;
