var EventEmitter = require('events').EventEmitter;
var TLRGEventListener = require('./../event/TLRGEventListener').TLRGEventListener;
var TLRGEventHandler = require('./../event/TLRGEventHandler').TLRGEventHandler;
var Logger = require('./../util/Logger').Logger;
var util = require('util');
var _ = require('underscore');

function HotelDetailsInterface(url, rest, tlrgAppId) {
  this.getHotelDetails = function(id) {
    if (!_(id).isNumber()) {
      throw new Error('id must be a number');
    }

    var hotelDetailsUrl = util.format('%s/hotel/%d', url, id);
    var emitter = new EventEmitter();
    var listener = new TLRGEventListener(emitter);
    var logger = new Logger();
    var handler = new TLRGEventHandler();

    logger.info('starting', hotelDetailsUrl);
    var restResult = rest.get(hotelDetailsUrl, {
      timeout: 5000,
      headers: {
        'TLRG-AppId': tlrgAppId
      }
    });

    handler.handle(restResult, emitter, hotelDetailsUrl);

    return listener;
  };
}

exports.HotelDetailsInterface = HotelDetailsInterface;
