var async = require('async');
var Logger = require('./util/Logger').Logger;
var EventEmitter = require('events').EventEmitter;
var util = require('util');
var _ = require('underscore');
var CacheWrapper = require('./cache/CacheWrapper').CacheWrapper;
var TLRGEventListener = require('./event/TLRGEventListener').TLRGEventListener;

function QueryByHotelID(interfaces, queueSize) {
  var validParameters = ['sDate', 'cur', 'adults', 'children', 'nights', 'lang', 'data'];
  var hotelDetailsCacheWrapper = new CacheWrapper('hotel-details');
  var ratesCacheWrapper = new CacheWrapper('rates');

  this.handleRequest = function(params) {
    if (!_(queueSize).isNumber() || isNaN(queueSize)) {
      new Logger().info('Using default queue size');
      queueSize = 10;
    }

    var emitter = new EventEmitter();
    var result = {};
    var listener = new TLRGEventListener(emitter);

    checkParameters(params);

    var q = async.queue(function(task, callback) {
      var key = util.format('%d_%s', task.id, task.task);

      executeTask(task).success(function(message) {
        result[key] = message;
        callback();
      }).timeout(function(timeout) {
        result[key] = {
          error: timeout
        };
        callback();
      }).fail(function(message) {
        result[key] = {
          fail: message
        };
        callback();
      }).error(function(message) {
        result[key] = {
          error: message
        };
        callback();
      });
    }, queueSize);

    q.pause();

    params.ids.forEach(function(id) {
      createTasksForHotel(q, id, params);
    });

    q.resume();

    q.drain = function() {
      emitter.emit('success', combineResults(params, result));
    };

    return listener;
  };

  var combineResults = function(params, result) {
    var combinedResults = {};
    combinedResults.parameters = params;
    params.ids.forEach(function(id) {
      combinedResults[id] = {};
      combinedResults[id].details = result[id + '_details'];
      combinedResults[id].rooms = result[id + '_rates'];
    });

    return combinedResults;
  };

  var checkParameters = function(params) {
    if (!(params.hasOwnProperty('ids')) || params.ids.length < 1) {
      new Logger().error('Expecting at least one hotel id', undefined, params);
      throw new Error('Expecting at least one hotel id');
    }

    params.ids.forEach(function(id) {
      if (!_.isNumber(id)) {
        throw new Error('hotels ids must be numbers');
      }
    });

    _(params.searchParameters).keys().forEach(function(x) {
      if (!_.contains(validParameters, x)) {
        throw new Error(util.format('Unknown parameter %s', x));
      }
    });
  };

  var makeCallback = function(id) {
    return function(err) {};
  };

  var createTasksForHotel = function(q, id, params) {
    q.push({
      id: id,
      task: 'details'
    }, makeCallback(id));
    q.push(makeRatesTask(id, params), makeCallback(id));
  };

  var makeRatesTask = function(id, params) {
    var adults = _(params.adults).isUndefined() ? 2 : params.adults;
    var children = _(params.children).isUndefined() ? 0 : params.children;

    var result = {
      id: id,
      task: 'rates',
      searchParameters: {
        d: params.sDate,
        cur: params.cur,
        n: params.nights
      }
    };

    result.searchParameters.o = (_.isUndefined(params.adults) && _.isUndefined(params.children)) ? undefined : util.format("%s-%s", adults, children);

    _(result.searchParameters).keys().forEach(function(k) {
      if (!result.searchParameters[k]) {
        delete result.searchParameters[k];
      }
    });

    return result;
  };

  var executeTask = function(task) {
    if (task.task === 'details') {
      return new hotelDetailsCacheWrapper.wrap(interfaces.hotelDetailsInterface.getHotelDetails, [task.id]);
    } else if (task.task === 'rates') {
      return ratesCacheWrapper.wrap(interfaces.ratesInterface.getRates, [task.id, task.searchParameters]);
    } else {
      throw new Error('Unknown task type: ' + task.task);
    }
  };
}

exports.QueryByHotelID = QueryByHotelID;
