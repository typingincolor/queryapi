var RedisCache = require('./RedisCache').RedisCache;
var ApigeeCache = require('./ApigeeCache').ApigeeCache;
var NullCache = require('./NullCache').NullCache;
var apigee = require('apigee-access');

CacheWrapper = function(cacheName) {
  if (apigee.getMode() === 'apigee') {
    return new ApigeeCache(cacheName);
  }
  else if (global.cacheType === 'redis') {
    return new RedisCache(cacheName);
  }
  else {
    return new NullCache();
  }
};

exports.CacheWrapper = CacheWrapper;
