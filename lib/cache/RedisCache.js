var _ = require('underscore');
var redis = require('redis');
var hash = require('object-hash');
var EventEmitter = require('events').EventEmitter;
var TLRGEventListener = require('./../event/TLRGEventListener').TLRGEventListener;
var util = require('util');
var nconf = require('nconf');

RedisCache = function(cacheName) {
  nconf.argv().env().file('./config/redis.cache.json');
  var tmpPort = parseInt(nconf.get('DB_PORT_6379_TCP_PORT'));
  var tmpIp = nconf.get('DB_PORT_6379_TCP_ADDR');
  var expiry = parseInt(nconf.get('expiryTime'));

  var redisPort = isNaN(tmpPort) ? 6379 : tmpPort;
  var redisIp = _(tmpIp).isUndefined() ? 'localhost' : tmpIp;

  if (!_(expiry).isNumber()) {
    throw new Error('expecting expiry time to be a number');
  }

  this.wrap = function(method, params) {
    if (!_(params).isArray()) {
      throw new Error('expecting parameters to be an array');
    }

    var cache = redis.createClient(redisPort, redisIp, {});
    var emitter = new EventEmitter();
    var listener = new TLRGEventListener(emitter);

    var key = util.format('%s:%s', cacheName, hash.sha1(params));

    cache.get(key, function(err, data) {
      if (_(data).isNull() || _(data).isUndefined()) {
        // nothing in the cache
        var eventListener = method.apply(this, params);

        eventListener.success(function(message) {
          cache.set(key, JSON.stringify(message), 'EX', expiry);
          message.cacheHit = false;
          emitter.emit('success', message);
        }).timeout(function(event) {
          event.cacheHit = false;
          emitter.emit('timeout', event);
        }).fail(function(event) {
          event.cacheHit = false;
          emitter.emit('fail', event);
        }).error(function(event) {
          event.cacheHit = false;
          emitter.emit('error', event);
        });
      } else {
        // there is a cached result
        var result = JSON.parse(data);
        result.cacheHit = true;
        emitter.emit('success', result);
      }
    });

    return listener;
  };
};

exports.RedisCache = RedisCache;
