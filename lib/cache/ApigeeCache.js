var _ = require('underscore');
var apigee = require('apigee-access');
var hash = require('object-hash');
var EventEmitter = require('events').EventEmitter;
var TLRGEventListener = require('./../event/TLRGEventListener').TLRGEventListener;
var nconf = require('nconf');

ApigeeCache = function (cacheName) {
  nconf.argv().env().file('./config/apigee.cache.json');
  var expiry = nconf.get('expiryTime');

  if (!_(expiry).isNumber()) {
    throw new Error('expecting expiry time to be a number');
  }

  this.wrap = function (method, params) {
    if (!_(params).isArray()) {
      throw new Error('expecting parameters to be an array');
    }

    var emitter = new EventEmitter();
    var listener = new TLRGEventListener(emitter);

    var cache = apigee.getCache(cacheName, {resource: cacheName});
    var key = hash.sha1(params);

    cache.get(key, function (err, data) {
      if (_(data).isUndefined()) {
        // nothing in the cache
        var eventListener = method.apply(this, params);

        eventListener.success(function (message) {
          cache.put(key, message, expiry);
          message.cacheHit = false;
          emitter.emit('success', message);
        }).timeout(function (event) {
          emitter.emit('timeout', event);
        }).fail(function (event) {
          emitter.emit('fail', event);
        }).error(function (event) {
          emitter.emit('error', event);
        });
      }
      else {
        // there is a cached result
        var result = JSON.parse(data);
        result.cacheHit = true;
        emitter.emit('success', result);
      }
    });

    return listener;
  }
};

exports.ApigeeCache = ApigeeCache;