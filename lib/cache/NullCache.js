var _ = require('underscore');

NullCache = function() {
  this.wrap = function(method, params) {
    if (!_(params).isArray()) {
      throw new Error('expecting parameters to be an array');
    }

    return method.apply(this, params);
  };
};

exports.NullCache = NullCache;
