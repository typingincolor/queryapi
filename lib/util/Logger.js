var winston = require('winston');

winston.add(winston.transports.File, {
  filename: 'winston.log'
});

function Logger() {
  this.info = function(message, url, params) {
    winston.info(message, {
      url: url,
      parameters: params
    });
  };

  this.error = function(message, url, params) {
    winston.error(message, {
      url: url,
      parameters: params
    });
  };

  this.startup = function(configuration) {
    winston.info('listening', {
      settings: configuration
    });
  };
}

exports.Logger = Logger;
