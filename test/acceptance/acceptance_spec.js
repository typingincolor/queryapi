var request = require('supertest');
var should = require('chai').should();
var _ = require('underscore');
var util = require('util');
var server;
var winston = require('winston');

winston.remove(winston.transports.Console);

beforeEach(function () {
  server = require('../../app').server;
  server.listen(8080);
});

afterEach(function () {
  server.close();
});

describe('QueryAPI', function () {
  describe('GET /queryapi', function () {
    it('can retrieve hotel details for a list of hotels', function (done) {
      var ids = [90019, 7248];

      this.timeout(15000);
      setTimeout(done, 15000);
      request(server)
        .get('/queryapi?ids=' + ids.join())
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          _.keys(res.body).length.should.equal(3);
          checkCorrectElementsPresent(res.body, ids);
          checkNoFailures(res.body, ids);
          done();
        });
    });

    it('recieves an error when a date in the past is passed in', function (done) {
      this.timeout(15000);
      setTimeout(done, 15000);

      var ids = [90019];

      var url = util.format('/queryapi?ids=%s&sDate=2014-01-01', ids.join());
      request(server)
        .get(url)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          _.keys(res.body).length.should.equal(2);
          checkCorrectElementsPresent(res.body, ids);

          _.keys(res.body[90019].rooms).should.deep.equal(['fail']);

          res.body[90019].rooms.fail.error.code.should.equal(4006);
          res.body[90019].rooms.fail.error.message.should.match(/^Arrival date of 01\/01\/2014 00:00:00 is invalid./);
          done();
        });
    });
  });
});

var checkCorrectElementsPresent = function (response, ids) {
  response.should.include.keys('parameters');
  ids.forEach(function (id) {
    response[id].should.include.keys('details');
    response[id].should.include.keys('rooms');
  });
};

var checkNoFailures = function (response, ids) {
  var validDetailsKeys = [ 'name',
    'location',
    'star_rating',
    'description',
    'facilities',
    'review_summary',
    'max_child_age',
    'check_in_out',
    'images',
    'important_info',
    'city_tax_info' ];

  ids.forEach(function (id) {
    response[id].details.should.include.keys(validDetailsKeys);
    response[id].rooms.should.include.keys('rooms');
  });
}
