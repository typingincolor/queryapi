var request = require('supertest');
var should = require('chai').should();
var results = require('./expected/ExpectedResult');
var server;

beforeEach(function() {
  server = require('../../app').server;
  global.cacheType = 'null';
  server.listen(8080);
});

afterEach(function() {
  server.close();
});

describe('QueryAPI', function() {
  describe('GET /queryapi', function() {
    it('can handle when one hotel ids is passed in', function(done) {
      this.timeout(15000);
      setTimeout(done, 15000);
      request(server)
        .get('/queryapi?ids=1')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          res.body.should.deep.equal(results.OkResponse);
          done();
        });
    }, 5000);

    it('can handle two hotel ids passed as individual params', function(done) {
      this.timeout(15000);
      setTimeout(done, 15000);
      request(server)
        .get('/queryapi?ids=1&ids=2')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          res.body.should.deep.equal(results.OkResponseTwoHotels);
          done();
        });
    }, 5000);

    it('can handle when three hotel ids are passed as a comma separated string', function(done) {
      this.timeout(15000);
      setTimeout(done, 15000);
      request(server)
        .get('/queryapi?ids=1,2,3')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          res.body.should.deep.equal(results.OkResponseThreeHotels);
          done();
        });
    }, 5000);

    it('returns error 500 when no hotel ids are passed in', function(done) {
      this.timeout(15000);
      setTimeout(done, 15000);
      request(server)
        .get('/queryapi')
        .expect(500)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          res.body.should.deep.equal({
            error: 'Expecting at least one hotel id'
          });
          done();
        });
    }, 5000);

    it('handles an sDate parameter correctly', function(done) {
      this.timeout(15000);
      setTimeout(done, 15000);
      request(server)
        .get('/queryapi?ids=1&sDate=2014-01-01')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          res.body.should.deep.equal(results.OkResponseSDate);
          done();
        });
    });
  });
});
