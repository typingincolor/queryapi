exports.OkResponse = {
  '1': {
    details: {
      id: 1,
      queryString: '',
      resource: 'hotel'
    },
    rooms: {
      id: 1,
      queryString: '',
      resource: 'rates'
    }
  },
  parameters: {
    ids: [1]
  }
};

exports.OkResponseTwoHotels = {
  '1': {
    details: {
      id: 1,
      queryString: '',
      resource: 'hotel'
    },
    rooms: {
      id: 1,
      queryString: '',
      resource: 'rates'
    }
  },
  '2': {
    details: {
      id: 2,
      queryString: '',
      resource: 'hotel'
    },
    rooms: {
      id: 2,
      queryString: '',
      resource: 'rates'
    }
  },
  parameters: {
    ids: [1,2]
  }
};

exports.OkResponseThreeHotels = {
  '1': {
    details: {
      id: 1,
      queryString: '',
      resource: 'hotel'
    },
    rooms: {
      id: 1,
      queryString: '',
      resource: 'rates'
    }
  },
  '2': {
    details: {
      id: 2,
      queryString: '',
      resource: 'hotel'
    },
    rooms: {
      id: 2,
      queryString: '',
      resource: 'rates'
    }
  },
  '3': {
    details: {
      id: 3,
      queryString: '',
      resource: 'hotel'
    },
    rooms: {
      id: 3,
      queryString: '',
      resource: 'rates'
    }
  },
  parameters: {
    ids: [1, 2, 3]
  }
};

exports.OkResponseSDate = {
  '1': {
    details: {
      id: 1,
      queryString: '',
      resource: 'hotel'
    },
    rooms: {
      id: 1,
      queryString: 'd=2014-01-01',
      resource: 'rates'
    }
  },
  parameters: {
    ids: [1],
    sDate: '2014-01-01'
  }
};
