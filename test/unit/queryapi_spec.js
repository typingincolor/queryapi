var QueryByHotelID = require('../../lib/QueryByHotelID').QueryByHotelID;
var MockTLRGInterface = require('../lib/MockTLRGInterface').MockTLRGInterface;
var MockTimeoutTLRGInterface = require('../lib/MockTimeoutTLRGInterface').MockTimeoutTLRGInterface;
var MockErrorTLRGInterface = require('../lib/MockErrorTLRGInterface').MockErrorTLRGInterface;
var MockFailTLRGInterface = require('../lib/MockFailTLRGInterface').MockFailTLRGInterface;
var TLRGInterface = require('../../lib/TLRGInterface').TLRGInterface;
var winston = require('winston');

winston.remove(winston.transports.Console);

var should = require('chai').should();

describe('QueryByHotelID', function() {
  describe('handleRequest()', function() {
    it('returns OK for a single hotel', function(done) {
      var params = {
        ids: [1]
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockTLRGInterface, new MockTLRGInterface));

      var expected = require('./expected/OK').OkSingleResponse;

      query.handleRequest(params).success(function(result) {
        result[1].details.should.deep.equal(expected[1].details);
        result[1].rooms.should.deep.equal(expected[1].rooms);
        done();
      });
    });

    it('returns OK', function(done) {
      var params = {
        ids: [1, 2, 3, 4, 5]
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockTLRGInterface, new MockTLRGInterface));

      var expected = require('./expected/OK').OkResponse;

      query.handleRequest(params).success(function(result) {
        result.should.deep.equal(expected);
        done();
      });
    });

    it('fails when empty array of hotel ids is passed in', function(done) {
      params = {
        ids: []
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockTLRGInterface, new MockTLRGInterface));

      query.handleRequest.bind(null, params).should.throw('Expecting at least one hotel id');
      done();
    });

    it('fails when no hotel ids are passed in', function(done) {
      params = {
        foo: []
      };

      var tlrgInterface = new MockTLRGInterface();
      var query = new QueryByHotelID(tlrgInterface);

      query.handleRequest.bind(null, params).should.throw('Expecting at least one hotel id');
      done();
    });

    it('handles a timeout correctly', function(done) {
      var params = {
        ids: [1]
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockTimeoutTLRGInterface, new MockTimeoutTLRGInterface));

      var expected = require('./expected/TLRGInterfaceResponses').SingleTimedOutResponse;

      query.handleRequest(params).success(function(result) {
        result.should.deep.equal(expected);
        done();
      });
    });

    it('handles a request when some of the backend responses timeout', function(done) {
      var params = {
        ids: [1, 2, 3, 4, 5]
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockTimeoutTLRGInterface, new MockTimeoutTLRGInterface));

      var expected = require('./expected/TLRGInterfaceResponses').TimedOutResponse;

      query.handleRequest(params).success(function(result) {
        result.should.deep.equal(expected);
        done();
      });
    });

    it('handles a fail response', function(done) {
      var params = {
        ids: [1]
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockFailTLRGInterface, new MockFailTLRGInterface));

      var expected = require('./expected/TLRGInterfaceResponses').SingleFailResponse;

      query.handleRequest(params).success(function(result) {
        result.should.deep.equal(expected);
        done();
      });
    });

    it('handles a request when some of the backend responses fail', function(done) {
      var params = {
        ids: [1, 2, 3, 4, 5]
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockFailTLRGInterface, new MockFailTLRGInterface));

      var expected = require('./expected/TLRGInterfaceResponses').FailResponse;

      query.handleRequest(params).success(function(result) {
        result.should.deep.equal(expected);
        done();
      });
    });

    it('handles an error response', function(done) {
      var params = {
        ids: [1]
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockErrorTLRGInterface, new MockErrorTLRGInterface));

      var expected = require('./expected/TLRGInterfaceResponses').SingleErrorResponse;

      query.handleRequest(params).success(function(result) {
        result.should.deep.equal(expected);
        done();
      });
    });

    it('handles a request when some of the backend responses error', function(done) {
      var params = {
        ids: [1, 2, 3, 4, 5]
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockErrorTLRGInterface, new MockErrorTLRGInterface));

      var expected = require('./expected/TLRGInterfaceResponses').ErrorResponse;

      query.handleRequest(params).success(function(result) {
        result.should.deep.equal(expected);
        done();
      });
    });

    it('handles query parameters correctly', function(done) {
      var params = {
        ids: [1],
        searchParameters: {
          sDate: '2014-01-01',
          data: 'product_info, rates',
          cur: 'GBP',
          adults: 1,
          children: 2,
          nights: 1,
          lang: 'en'
        }
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockTLRGInterface, new MockTLRGInterface));

      var expected = require('./expected/OK').OkParameterResponse;

      query.handleRequest(params).success(function(result) {
        result.should.deep.equal(expected);
        done();
      });
    });

    it('handles an invalid search parameter', function(done) {
      var params = {
        ids: [1],
        searchParameters: {
          sDate: '2014-01-01',
          xyzzy: 'invalid parameter'
        }
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockTLRGInterface, new MockTLRGInterface));

      query.handleRequest.bind(null, params).should.throw('Unknown parameter xyzzy');
      done();
    });

    it('throws an error when there is an invalid hotel id', function(done) {
      var params = {
        ids: [1, 'x'],
        searchParameters: {
          sDate: '2014-01-01',
          xyzzy: 'invalid parameter'
        }
      };

      var query = new QueryByHotelID(new TLRGInterface(new MockTLRGInterface, new MockTLRGInterface));

      query.handleRequest.bind(null, params).should.throw('hotels ids must be numbers');
      done();
    });
  });
});
