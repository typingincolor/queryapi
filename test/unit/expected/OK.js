exports.OkSingleResponse = {
  parameters: {
    ids: [1]
  },
  1: {
    details: {
      id: 1
    },
    rooms: {
      id: 1
    }
  }
};

exports.OkResponse = {
  parameters: {
    ids: [1, 2, 3, 4, 5]
  },
  1: {
    details: {
      id: 1
    },
    rooms: {
      id: 1
    }
  },
  2: {
    details: {
      id: 2
    },
    rooms: {
      id: 2
    }
  },
  3: {
    details: {
      id: 3
    },
    rooms: {
      id: 3
    }
  },
  4: {
    details: {
      id: 4
    },
    rooms: {
      id: 4
    }
  },
  5: {
    details: {
      id: 5
    },
    rooms: {
      id: 5
    }
  }
};

exports.OkParameterResponse = {
  parameters: {
    ids: [1],
    searchParameters: {
      sDate: '2014-01-01',
      data: 'product_info, rates',
      cur: 'GBP',
      adults: 1,
      children: 2,
      nights: 1,
      lang: 'en'
    }
  },
  1: {
    details: {
      id: 1
    },
    rooms: {
      id: 1
    }
  }
};
