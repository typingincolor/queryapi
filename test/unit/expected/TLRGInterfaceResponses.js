exports.SingleTimedOutResponse = {
  parameters: {
    ids: [1]
  },
  1: {
    details: {
      error: {
        timeout: 1000
      }
    },
    rooms: {
      error: {
        timeout: 1000
      }
    }
  }
};

exports.TimedOutResponse = {
  parameters: {
    ids: [1, 2, 3, 4, 5]
  },
  1: {
    details: {
      error: {
        timeout: 1000
      }
    },
    rooms: {
      error: {
        timeout: 1000
      }
    }
  },
  3: {
    details: {
      error: {
        timeout: 1000
      }
    },
    rooms: {
      error: {
        timeout: 1000
      }
    }
  },
  5: {
    details: {
      error: {
        timeout: 1000
      }
    },
    rooms: {
      error: {
        timeout: 1000
      }
    }
  },
  2: {
    details: {
      id: 2
    },
    rooms: {
      id: 2
    }
  },
  4: {
    details: {
      id: 4
    },
    rooms: {
      id: 4
    }
  }
};

exports.SingleFailResponse = {
  parameters: {
    ids: [1]
  },
  1: {
    details: {
      fail: {
        error: 'failed'
      }
    },
    rooms: {
      fail: {
        error: 'failed'
      }
    }
  }
};

exports.FailResponse = {
  parameters: {
    ids: [1, 2, 3, 4, 5]
  },
  1: {
    details: {
      fail: {
        error: 'failed'
      }
    },
    rooms: {
      fail: {
        error: 'failed'
      }
    }
  },
  3: {
    details: {
      fail: {
        error: 'failed'
      }
    },
    rooms: {
      fail: {
        error: 'failed'
      }
    }
  },
  5: {
    details: {
      fail: {
        error: 'failed'
      }
    },
    rooms: {
      fail: {
        error: 'failed'
      }
    }
  },
  2: {
    details: {
      id: 2
    },
    rooms: {
      id: 2
    }
  },
  4: {
    details: {
      id: 4
    },
    rooms: {
      id: 4
    }
  }
};

exports.SingleErrorResponse = {
  parameters: {
    ids: [1]
  },
  1: {
    details: {
      error: {
        error: 'error'
      }
    },
    rooms: {
      error: {
        error: 'error'
      }
    }
  }
};

exports.ErrorResponse = {
  parameters: {
    ids: [1, 2, 3, 4, 5]
  },
  1: {
    details: {
      error: {
        error: 'error'
      }
    },
    rooms: {
      error: {
        error: 'error'
      }
    }
  },
  3: {
    details: {
      error: {
        error: 'error'
      }
    },
    rooms: {
      error: {
        error: 'error'
      }
    }
  },
  5: {
    details: {
      error: {
        error: 'error'
      }
    },
    rooms: {
      error: {
        error: 'error'
      }
    }
  },
  2: {
    details: {
      id: 2
    },
    rooms: {
      id: 2
    }
  },
  4: {
    details: {
      id: 4
    },
    rooms: {
      id: 4
    }
  }
};
