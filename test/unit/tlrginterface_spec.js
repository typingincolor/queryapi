var HotelDetailsInterface = require('../../lib/interface/HotelDetailsInterface').HotelDetailsInterface;
var RatesInterface = require('../../lib/interface/RatesInterface').RatesInterface;
var MockRestler = require('../lib/MockRestler').MockRestler;
var mockRestler = new MockRestler();

var should = require('chai').should();

describe('HoteDetailsInterface', function() {
  describe('getHotelDetails()', function () {
    it('handles an OK response', function (done) {
      var testInterface = new HotelDetailsInterface('ok', mockRestler);

      var expected = {
        id: '1'
      };

      testInterface.getHotelDetails(1).success(function (message) {
        message.should.deep.equal(expected);
        done();
      });
    });

    it('handles a fail response', function (done) {
      var testInterface = new HotelDetailsInterface('fail', mockRestler);

      var expected = {
        fail: 'hotel id 1 failed'
      };

      testInterface.getHotelDetails(1).fail(function (message) {
        message.should.deep.equal(expected);
        done();
      });
    });

    it('handles a timeout response', function (done) {
      var testInterface = new HotelDetailsInterface('timeout', mockRestler);

      var expected = {
        timeout: 100
      };

      testInterface.getHotelDetails(1).timeout(function (message) {
        message.should.deep.equal(expected);
        done();
      });
    });

    it('handles an error response', function (done) {
      var testInterface = new HotelDetailsInterface('error', mockRestler);

      var expected = {
        url: 'error/hotel/1',
        error: {
          code: 'ENOTFOUND',
          errno: 'ENOTFOUND',
          syscall: 'getaddrinfo'
        }
      };

      testInterface.getHotelDetails(1).error(function (message) {
        message.should.deep.equal(expected);
        done();
      });
    });

    it('throws an error if an invalid hotel id is passed in', function(done) {
      var testInterface = new HotelDetailsInterface('ok', mockRestler);

      testInterface.getHotelDetails.bind(null, 'x').should.throw('id must be a number');
      done();
    });
  });
});

describe('RatesInterface', function() {
  describe('getRates()', function() {
    it('handles an OK response', function(done) {
      var testInterface = new RatesInterface('ok', mockRestler);

      var expected = {
        id: '1'
      };

      testInterface.getRates(1).success(function(message) {
        message.should.deep.equal(expected);
        done();
      });
    });

    it('handles a fail response', function(done) {
      var testInterface = new RatesInterface('fail', mockRestler);

      var expected = {
        fail: 'hotel id 1 failed'
      };

      testInterface.getRates(1).fail(function(message) {
        message.should.deep.equal(expected);
        done();
      });
    });

    it('handles a timeout response', function(done) {
      var testInterface = new RatesInterface('timeout', mockRestler);

      var expected = {
        timeout: 100
      };

      testInterface.getRates(1).timeout(function(message) {
        message.should.deep.equal(expected);
        done();
      });
    });

    it('handles an error response', function(done) {
      var testInterface = new RatesInterface('error', mockRestler);

      var expected = {
        url: 'error/hotel/1/rates',
        error: {
          code: 'ENOTFOUND',
          errno: 'ENOTFOUND',
          syscall: 'getaddrinfo'
        }
      };

      testInterface.getRates(1).error(function(message) {
        message.should.deep.equal(expected);
        done();
      });
    });

    it('throws an error if an invalid hotel id is passed in', function(done) {
      var testInterface = new RatesInterface('ok', mockRestler);

      testInterface.getRates.bind(null, 'x').should.throw('id must be a number');
      done();
    });
  });
});
