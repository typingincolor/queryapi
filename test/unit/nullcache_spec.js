var NullCache = require('../../lib/cache/NullCache').NullCache;
var EventEmitter = require('events').EventEmitter;
var TLRGEventListener = require('../../lib/event/TLRGEventListener').TLRGEventListener;
var should = require('chai').should();

var cache = new NullCache();

describe('NullCache', function () {
  describe('wrap', function () {
    it('returns an OK result', function (done) {
      var hotelId = Math.floor((Math.random() * 100000) + 1);

      cache.wrap(event, [hotelId, 'success'])
        .success(function (res) {
          res.id.should.equal(hotelId);
          done();
        });
    });

    it('handles a timeout', function (done) {
      var hotelId = Math.floor((Math.random() * 100000) + 1);

      cache.wrap(event, [hotelId, 'timeout'])
        .timeout(function (res) {
          res.id.should.equal(hotelId);
          done()
        });
    });

    it('handles a fail', function (done) {
      var hotelId = Math.floor((Math.random() * 100000) + 1);

      cache.wrap(event, [hotelId, 'fail'])
        .fail(function (res) {
          res.id.should.equal(hotelId);
          done()
        });
    });

    it('handles an error', function (done) {
      var hotelId = Math.floor((Math.random() * 100000) + 1);

      cache.wrap(event, [hotelId, 'error'])
        .error(function (res) {
          res.id.should.equal(hotelId);
          done()
        });
    });
  });

  var event = function (id, message) {
    var emitter = new EventEmitter();
    var self = new TLRGEventListener(emitter);
    setTimeout(function () {
      process.nextTick(function () {
        emitter.emit(message, {
          message: message,
          id: id
        });
      })
    }, 25);

    return self;
  };
});
