var EventEmitter = require('events').EventEmitter;
var TLRGEventListener = require('../../lib/event/TLRGEventListener').TLRGEventListener;

function MockTimeoutTLRGInterface() {
  this.getHotelDetails = function(id) {
    return emit(id);
  };

  this.getRates = function(id) {
    return emit(id);
  };

  var emit = function(id) {
    var emitter = new EventEmitter();
    var self = new TLRGEventListener(emitter);
    setTimeout(function() {
      process.nextTick(function() {
        if (id % 2 === 0) {
          emitter.emit('success', {
            id: id
          });
        } else {
          emitter.emit('timeout', {
            timeout: 1000
          });
        }
      });
    }, 25);

    return self;
  };
}

exports.MockTimeoutTLRGInterface = MockTimeoutTLRGInterface;
