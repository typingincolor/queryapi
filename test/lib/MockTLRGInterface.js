var EventEmitter = require('events').EventEmitter;
var TLRGEventListener = require('../../lib/event/TLRGEventListener').TLRGEventListener;
var _ = require('underscore');

function MockTLRGInterface() {
  this.getHotelDetails = function(id) {
    var emitter = new EventEmitter();
    var self = new TLRGEventListener(emitter);
    setTimeout(function() {
      process.nextTick(function() {
        emitter.emit('success', {
          id: id,
        });
      });
    }, 25);

    return self;
  };

  this.getRates = function(id, params) {
    var emitter = new EventEmitter();
    var self = new TLRGEventListener(emitter);
    setTimeout(function() {
      process.nextTick(function() {
        var event = {
          id: id,
          sDate: params.sDate,
          data: params.data,
          cur: params.cur,
          adults: params.adults,
          children: params.children,
          nights: params.nights,
          lang: params.lang
        };

        _.keys(event).forEach(function(k) {
          if (!event[k]) {
            delete event[k];
          }
        });

        emitter.emit('success', event);
      });
    }, 25);

    return self;
  };
}

exports.MockTLRGInterface = MockTLRGInterface;
