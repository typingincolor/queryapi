var EventEmitter = require('events').EventEmitter;

function MockRestler() {
  this.get = function(url, params) {
    var emitter = new EventEmitter();
    setTimeout(function() {
      switch (true) {
        case /^ok/.test(url):
          emit(emitter, 'success', {
            id: '1'
          });
          break;
        case /^fail/.test(url):
          emit(emitter, 'fail', {
            fail: 'hotel id 1 failed'
          });
          break;
        case /^error/.test(url):
          emit(emitter, 'error', {
            code: 'ENOTFOUND',
            errno: 'ENOTFOUND',
            syscall: 'getaddrinfo'
          });
          break;
        case /^timeout/.test(url):
          emit(emitter, 'timeout', 100);
          break;
        default:
          console.error('unknown url: %s', url);
          emit(emitter, 'error', {
            error: 'unknown url ' + url
          });
          break;
      }
    }, 25);

    return emitter;
  };

  var emit = function(emitter, event, message) {
    process.nextTick(function() {
      emitter.emit(event, message);
    });
  };
}

exports.MockRestler = MockRestler;
