var RedisCache = require('../../lib/cache/RedisCache').RedisCache;
var MockTLRGInterface = require('../lib/MockTLRGInterface').MockTLRGInterface;
var EventEmitter = require('events').EventEmitter;
var TLRGEventListener = require('../../lib/event/TLRGEventListener').TLRGEventListener;
var should = require('chai').should();
var async = require('async');
var _ = require('underscore');

process.env.expiryTime = 5;

var cache = new RedisCache("test");
var tlrgInterface = new MockTLRGInterface();

describe('RedisCache', function () {
  describe('wrap', function () {
    it('returns a cached result', function (done) {
      var hotelId = _.random(1, 100000);

      async.series([
        function (callback) {
          cache.wrap(tlrgInterface.getHotelDetails, [hotelId])
            .success(function (res) {
              res.cacheHit.should.be.false;
              res.id.should.equal(hotelId);
              callback(null);
            });
        },
        function (callback) {
          cache.wrap(tlrgInterface.getHotelDetails, [hotelId])
            .success(function (res) {
              res.cacheHit.should.be.true;
              res.id.should.equal(hotelId);
              callback(null);
            });
        }
      ], function (err) {
        done();
      });
    });

    it('it does not cache a timeout', function (done) {
      var hotelId = _.random(1, 100000);

      async.series([
        function (callback) {
          cache.wrap(event, [hotelId, 'timeout'])
            .timeout(function (res) {
              res.cacheHit.should.be.false;
              res.id.should.equal(hotelId);
              res.message.should.equal('timeout');
              callback(null);
            });
        },
        function (callback) {
          cache.wrap(tlrgInterface.getHotelDetails, [hotelId])
            .success(function (res) {
              res.cacheHit.should.be.false;
              res.id.should.equal(hotelId);
              callback(null);
            });
        }
      ], function (err) {
        done();
      });
    });

    it('it does not cache a fail', function (done) {
      var hotelId = _.random(1, 100000);

      async.series([
        function (callback) {
          cache.wrap(event, [hotelId, 'fail'])
            .fail(function (res) {
              res.cacheHit.should.be.false;
              res.id.should.equal(hotelId);
              res.message.should.equal('fail');
              callback(null);
            });
        },
        function (callback) {
          cache.wrap(tlrgInterface.getHotelDetails, [hotelId])
            .success(function (res) {
              res.cacheHit.should.be.false;
              res.id.should.equal(hotelId);
              callback(null);
            });
        }
      ], function (err) {
        done();
      });
    });

    it('it does not cache an error', function (done) {
      var hotelId = _.random(1, 100000);

      async.series([
        function (callback) {
          cache.wrap(event, [hotelId, 'error'])
            .error(function (res) {
              res.cacheHit.should.be.false;
              res.id.should.equal(hotelId);
              res.message.should.equal('error');
              callback(null);
            });
        },
        function (callback) {
          cache.wrap(tlrgInterface.getHotelDetails, [hotelId])
            .success(function (res) {
              res.cacheHit.should.be.false;
              res.id.should.equal(hotelId);
              callback(null);
            });
        }
      ], function (err) {
        done();
      });
    });
  });

  var event = function (id, message) {
    var emitter = new EventEmitter();
    var self = new TLRGEventListener(emitter);
    setTimeout(function () {
      process.nextTick(function () {
        emitter.emit(message, {
          message: message,
          id: id
        });
      })
    }, 25);

    return self;
  };
});
