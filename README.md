# README #

## Starting the API

`npm start`

sample request: `http://localhost:8080/queryapi?ids=1,2,3`

## Running the unit tests

`npm test`

## Running the integration tests

`mocha test/integration`

## Running the redis tests

`mocha test/redis`

## Running the acceptance tests

    export url={tlrg api url}
    export tlrgAppId={tlrg api key}
    mocha test/acceptance

## Setting the cache type

either change the `cacheType` setting in `config/config.json` or `export cacheType={cache type}`

valid settings are `redis`, everything else defaults to the null cache.

If the api is running on apigee, then it will always use the apigee caches.

## Configuration Files

Look in `config` directory

## CI Status

[ ![Travis Status for typingincolor/queryapi](https://api.travis-ci.org/typingincolor/queryapi.svg?branch=master)](https://travis-ci.org/typingincolor/queryapi)

## Code Climate

[![Code Climate](https://codeclimate.com/github/typingincolor/queryapi/badges/gpa.svg)](https://codeclimate.com/github/typingincolor/queryapi)
