var QueryByHotelID = require('./lib/QueryByHotelID').QueryByHotelID;
var HotelDetailsInterface = require('./lib/interface/HotelDetailsInterface').HotelDetailsInterface;
var RatesInterface = require('./lib/interface/RatesInterface').RatesInterface;
var TLRGInterface = require('./lib/TLRGInterface').TLRGInterface;
var Logger = require('./lib/util/Logger').Logger;
var restify = require('restify');
var rest = require('restler');
var _ = require('underscore');
var apigee = require('apigee-access');

var server = restify.createServer();
server.use(restify.queryParser());

var logger = new Logger();

var nconf = require('nconf');
nconf.argv().env().file('./config/config.json');

global.cacheType = nconf.get('cacheType');

global.nconf = nconf;

var isApigeeStandalone = function() {
  return apigee.getMode() === 'standalone';
};

var CacheWrapper = require('./lib/cache/CacheWrapper').CacheWrapper;
var wrapper = new CacheWrapper('query');

server.get('/queryapi', function(req, res) {
  var url = isApigeeStandalone() ? nconf.get('url') : apigee.getVariable(req, 'configuration.url');
  var tlrgAppId = isApigeeStandalone() ? nconf.get('tlrgAppId') : apigee.getVariable(req, 'configuration.tlrgAppId');
  var tmpQueueSize = isApigeeStandalone() ? nconf.get('queueSize') : apigee.getVariable(req, 'configuration.queueSize');

  var queueSize = parseInt(tmpQueueSize);

  var hotelDetailsInterface = new HotelDetailsInterface(url, rest, tlrgAppId);
  var ratesInterface = new RatesInterface(url, rest, tlrgAppId);

  var query = new QueryByHotelID(new TLRGInterface(hotelDetailsInterface, ratesInterface), queueSize);

  if (!_.has(req.query, 'ids')) {
    res.json(500, {
      error: 'Expecting at least one hotel id'
    });
  }

  var searchParameters = _(req.query).clone();

  searchParameters.ids = _(searchParameters.ids).isArray() ? searchParameters.ids : searchParameters.ids.split(',');
  var integerIds = searchParameters.ids.map(Number);
  integerIds.sort(function(a, b) {
    return a - b;
  });
  searchParameters.ids = integerIds;

  wrapper.wrap(query.handleRequest, [searchParameters]).success(function(result) {
    res.json(result);
  });
});

server.listen(8080, function() {
  logger.startup({
    server: {
      name: server.name,
      url: server.url
    },
    mode: process.env.NODE_ENV,
    apigeeMode: apigee.getMode()
  });
});

module.exports.server = server;
